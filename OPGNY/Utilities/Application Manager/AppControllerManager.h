//
//  AppControllerManager.h
//  OPGNY
//
//  Created by Nishat Ansari on 05/06/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BudgetSectionViewController.h"
#import "FirstSelectionVC.h"
#import "HomeVC.h"
#import "LocationSelectionViewController.h"
#import "LoginVC.h"
#import "MyProfileVC.h"
#import "PriceSelectionViewController.h"
#import "SelectVC.h"
#import "SizeSelectionViewController.h"

@interface AppControllerManager : NSObject

@property (retain, nonatomic) BudgetSectionViewController *budgetSectionViewController;
@property (retain, nonatomic) FirstSelectionVC *firstSelectionVC;
@property (retain, nonatomic) HomeVC *homeVC;
@property (retain, nonatomic) LocationSelectionViewController *locationSelectionViewController;
@property (retain, nonatomic) LoginVC *loginVC;
@property (retain, nonatomic) MyProfileVC *myProfileVC;
@property (retain, nonatomic) PriceSelectionViewController *priceSelectionViewController;
@property (retain, nonatomic) SelectVC *selectVC;
@property (retain, nonatomic) SizeSelectionViewController *sizeSelectionViewController;


-(void)initiateViewControllers;

@end
