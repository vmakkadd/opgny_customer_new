//
//  ApplicationManager.m
//  OPGNY
//
//  Created by Nilesh Pal on 12/02/17.
//  Copyright © 2017 Nilesh Pal. All rights reserved.
//


#import "ApplicationManager.h"

@implementation ApplicationManager

+ (id)sharedManagerInstance {
    
    static ApplicationManager *sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

#pragma mark - Validation Methods

- (BOOL)isNetworkConnected {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

- (BOOL)emailAddressIsValid:(NSString *)emailAddress {
    
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:emailAddress];
}

- (BOOL)mobileNumberValidator:(NSString *)phone {
    
    NSError *error = NULL;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
    
    NSRange inputRange = NSMakeRange(0, [phone length]);
    NSArray *matches = [detector matchesInString:phone options:0 range:inputRange];
    
    // no match at all
    if ([matches count] == 0) {
        return NO;
    }
    
    // found match but we need to check if it matched the whole string
    NSTextCheckingResult *result = (NSTextCheckingResult *)[matches objectAtIndex:0];
    
    if ([result resultType] == NSTextCheckingTypePhoneNumber && result.range.location == inputRange.location && result.range.length == inputRange.length) {
        // it matched the whole string
        return YES;
    }
    else {
        // it only matched partial string
        return NO;
    }
}

#pragma mark - Integar in Comma seprated

-(NSString*)setAmountInCommaSeperated:(NSString*)amount
{
    NSNumberFormatter *formatter;
    if (formatter) {
        formatter = [NSNumberFormatter new];
    }
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important! to get commas (or locale equivalent)
    [formatter setMaximumFractionDigits:0]; // to avoid any decimal
    
    NSString *formatted = [formatter stringFromNumber:[NSNumber numberWithInteger:[amount integerValue]]];
    return formatted;
}


#pragma mark - Show Alert

-(void)showAlertWithMessage:(NSString *)message  {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:okAction];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
    
}

-(void)showAlertWithMessage:(NSString *)message handler:(void (^ __nullable)(UIAlertAction *action))handler {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:handler];
    [alertController addAction:okAction];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
}

-(void)showAlertWithMessageForAction:(NSString *)message withActionButtonName:(NSString*)actionButtonName withCancelButtonName:(NSString*)cancelButtonAcion handler:(void (^ __nullable)(UIAlertAction *action))handler  {
    
    UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:alertTitle message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionButtonName style:UIAlertActionStyleDefault handler:handler];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonAcion style:UIAlertActionStyleCancel handler:nil];
    [alertViewController addAction:okAction];
    [alertViewController addAction:cancelAction];
    
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertViewController animated:YES completion:nil];
}


#pragma mark - WebAPIs
- (void)callPOSTWebServiceFormData:(NSDictionary *)postData andURL:(NSString *)urlString andView:(UIView *)view andgetData:(StringConsumer) consumer {
    
    NSLog(@"Res Str %@",postData);
    NSLog(@"URL - %@",urlString);
    if ([self isNetworkConnected]) {
        
            NSMutableURLRequest *request = [self postWith:postData url:urlString];
        [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                         completionHandler:
          ^(NSData *data, NSURLResponse *response, NSError *error) {
              
              dispatch_async(dispatch_get_main_queue(), ^{
                NSString *res = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                  NSLog(@"Res Str %@",res);
                  
                  res=[res stringByReplacingOccurrencesOfString:@":null" withString:@":\"\""];
                  NSData *dataNew = [res dataUsingEncoding:NSUTF8StringEncoding];
                  
                  if ([(NSHTTPURLResponse *)response statusCode]==200)
                  {
                      
                      NSError* error;
                      NSDictionary *json = [NSJSONSerialization
                                            JSONObjectWithData:dataNew
                                            options:kNilOptions
                                            error:&error];
                      
                      consumer(json,nil);
                  }
                  else
                  {
                      consumer(nil,error);
                  }
              });
              
          }] resume];
    }
    else {
        [self showAlertWithMessage:networkNotConnected];
    }
}


- (void)callPOSTWebService:(NSDictionary *)postData andURL:(NSString *)urlString andHeader:(BOOL)isHeader andView:(UIView *)view andgetData:(StringConsumer) consumer {
    
    if ([self isNetworkConnected]) {
        [MBProgressHUD showHUDAddedTo:view animated:YES];
        NSData *postJsonData;
        NSString *jsonString;
        
        if([NSJSONSerialization isValidJSONObject:postData])
        {
            postJsonData = [NSJSONSerialization dataWithJSONObject:postData options:0 error:nil];
            jsonString = [[NSString alloc]initWithData:postJsonData encoding:NSUTF8StringEncoding];
        }
        
//        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"%@\r\n", dict[param]] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:120];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postJsonData];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                         completionHandler:
          ^(NSData *data, NSURLResponse *response, NSError *error) {
              
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [MBProgressHUD hideHUDForView:view animated:YES];
                  NSString *res = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                  NSLog(@"Res Str %@",res);
                  
                  res=[res stringByReplacingOccurrencesOfString:@":null" withString:@":\"\""];
                  NSData *dataNew = [res dataUsingEncoding:NSUTF8StringEncoding];
                  
                  if ([(NSHTTPURLResponse *)response statusCode]==200)
                  {
                      
                      NSError* error;
                      NSDictionary *json = [NSJSONSerialization
                                            JSONObjectWithData:dataNew
                                            options:kNilOptions
                                            error:&error];
                      
                      consumer(json,nil);
                  }
                  else
                  {
                      consumer(nil,error);
                  }
              });
              
          }] resume];
    }
    else {
        [self showAlertWithMessage:networkNotConnected];
    }
}

- (void)callGETWebService:(NSString *)urlString andView:(UIView *)view andgetData:(StringConsumer) consumer {
    
    if ([self isNetworkConnected]) {
        
        [MBProgressHUD showHUDAddedTo:view animated:YES];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:120];
        [request setHTTPMethod:@"GET"];

        [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                         completionHandler:
          ^(NSData *data, NSURLResponse *response, NSError *error) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  [MBProgressHUD hideHUDForView:view animated:YES];
                  NSString *res = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                  NSLog(@"Res Str %@",res);

                  if ([(NSHTTPURLResponse *)response statusCode]==200)
                  {
                      
                      NSError* error;
                      NSDictionary *json = [NSJSONSerialization
                                            JSONObjectWithData:data
                                            options:kNilOptions
                                            error:&error];
                      
                      consumer(json,nil);
                  }
                  else
                  {
                      consumer(nil,error);
                  }
              });
          }] resume];
    }
    else {
        [self showAlertWithMessage:networkNotConnected];
    }
    
}

- (void)callMultipartWebService:(NSDictionary *)postData andURL:(NSString *)urlString andView:(UIView *)view  andgetData:(StringConsumer) consumer {
    
    if ([self isNetworkConnected]) {
//        [MBProgressHUD showHUDAddedTo:view animated:YES];
        NSMutableURLRequest *request = [self multipartRequestWithURL:[NSURL URLWithString:urlString] andData:postData];
        
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                         completionHandler:
          ^(NSData *data, NSURLResponse *response, NSError *error) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  [MBProgressHUD hideHUDForView:view animated:YES];
                  
                  NSString *res = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                  NSLog(@"Res Str %@",res);
                  
                  res=[res stringByReplacingOccurrencesOfString:@"null" withString:@"\"\""];
                  NSData *dataNew = [res dataUsingEncoding:NSUTF8StringEncoding];
                  
                  if ([(NSHTTPURLResponse *)response statusCode]==200)
                  {
                      
                      NSError* error;
                      NSDictionary *json = [NSJSONSerialization
                                            JSONObjectWithData:dataNew
                                            options:kNilOptions
                                            error:&error];
                      
                      consumer(json,nil);
                  }
                  else
                  {
                      consumer(nil,error);
                  }
              });
          }] resume];
    }
    else {
        [self showAlertWithMessage:networkNotConnected];
    }
  
}


#pragma mark - Multipart Data

- (NSMutableURLRequest *) multipartRequestWithURL:(NSURL *)url andData:(NSDictionary *) dict
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSDate *dt = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
    int timestamp = [dt timeIntervalSince1970];
    
    NSString *boundary = [NSString stringWithFormat:@"BOUNDARY-%d-%@", timestamp, [[NSProcessInfo processInfo] globallyUniqueString]];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    
    for (NSString *param in dict) {
        if ([param isEqualToString:@"profile_picture"] ) {
            NSDate *time = [NSDate date];
            NSDateFormatter* df = [NSDateFormatter new];
            [df setDateFormat:@"ddMMyyHHmmss"];
            NSString *timeString = [df stringFromDate:time];
            NSString *fileName = [NSString stringWithFormat:@"%@.jpg", timeString];
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",param,fileName] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:UIImageJPEGRepresentation(dict[param], 0.4)];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        else {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", dict[param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
     NSLog(@"Request body %@", [[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding]);
    // set URL
    [request setURL:url];
    
    return request;
}

#pragma mark - WhiteSpaces

- (NSString *)removeWhiteSpace:(NSString *)str {
    return [str stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
}



- (NSMutableURLRequest *)postWith:(NSDictionary *)post_vars url:(NSString *)urlStr
{
    NSString *urlString = urlStr;
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSString *boundary = @"----1010101010";
    
    //  define content type and add Body Boundry
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSEnumerator *enumerator = [post_vars keyEnumerator];
    NSString *key;
    NSString *value;
    NSString *content_disposition;
    
    while ((key = (NSString *)[enumerator nextObject])) {
        
        if ([key isEqualToString:@"file"]) {
            
            value = (NSString *)[post_vars objectForKey:key];
            //  ***     Write Your Image Name Here  ***
            // Covnert image to Data and bind to your post request
            NSData *postData = UIImageJPEGRepresentation([UIImage imageNamed:@"yourImage"], 1.0);
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\";\r\nfilename=\"testUploadFile.jpg\"\r\n\r\n",value] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:postData];
        } else {
            value = (NSString *)[post_vars objectForKey:key];
            
            content_disposition = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key];
            [body appendData:[content_disposition dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[value dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    //Close the request body with Boundry
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    return request;
}


@end
