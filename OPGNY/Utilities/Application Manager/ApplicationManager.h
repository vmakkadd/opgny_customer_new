//
//  ApplicationManager.h
//  OPGNY
//
//  Created by Nilesh Pal on 12/02/17.
//  Copyright © 2017 Nilesh Pal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^StringConsumer)(NSDictionary* , NSError *);

@interface ApplicationManager : NSObject

+ (id)sharedManagerInstance;

- (BOOL)isNetworkConnected;
- (BOOL)emailAddressIsValid:(NSString *)emailAddress;
- (BOOL)mobileNumberValidator:(NSString *)phone;
- (NSString*)setAmountInCommaSeperated:(NSString*)amount;

-(void)showAlertWithMessage:(NSString *)message handler:(void (^ __nullable)(UIAlertAction *action))handler;
-(void)showAlertWithMessage:(NSString *)message;
-(void)showAlertWithMessageForAction:(NSString *)message withActionButtonName:(NSString*)actionButtonName withCancelButtonName:(NSString*)cancelButtonAcion handler:(void (^ __nullable)(UIAlertAction *action))handler;

- (NSString *)removeWhiteSpace:(NSString *)str;

- (void)callPOSTWebService:(NSDictionary *)postData andURL:(NSString *)urlString andHeader:(BOOL)isHeader andView:(UIView *)view andgetData:(StringConsumer) consumer;
- (void)callGETWebService:(NSString *)urlString andView:(UIView *)view andgetData:(StringConsumer) consumer;
- (void)callMultipartWebService:(NSDictionary *)postData andURL:(NSString *)urlString andView:(UIView *)view  andgetData:(StringConsumer) consumer;
- (NSMutableURLRequest *) multipartRequestWithURL:(NSURL *)url andData:(NSDictionary *) dict;
- (void)callPOSTWebServiceFormData:(NSDictionary *)postData andURL:(NSString *)urlString andView:(UIView *)view andgetData:(StringConsumer) consumer ;
@end
