//
//  AppControllerManager.m
//  OPGNY
//
//  Created by Nishat Ansari on 05/06/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import "AppControllerManager.h"

@implementation AppControllerManager

-(void)initiateViewControllers
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    _budgetSectionViewController             = [sb instantiateViewControllerWithIdentifier:@"BudgetSectionViewController"];
    _firstSelectionVC                        = [sb instantiateViewControllerWithIdentifier:@"FirstSelectionVC"];
    _homeVC                                  = [sb instantiateViewControllerWithIdentifier:@"HomeVC"];
    _locationSelectionViewController         = [sb instantiateViewControllerWithIdentifier:@"LocationSelectionViewController"];
    _loginVC                                 = [sb instantiateViewControllerWithIdentifier:@"LoginVC"];
    _myProfileVC                             = [sb instantiateViewControllerWithIdentifier:@"MyProfileVC"];
    _priceSelectionViewController            = [sb instantiateViewControllerWithIdentifier:@"PriceSelectionViewController"];
    _selectVC                                = [sb instantiateViewControllerWithIdentifier:@"SelectVC"];
    _sizeSelectionViewController             = [sb instantiateViewControllerWithIdentifier:@"SizeSelectionViewController"];
    
}

@end
