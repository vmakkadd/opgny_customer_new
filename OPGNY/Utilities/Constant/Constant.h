//
//  Constant.h
//  OPGNY
//
//  Created by Nilesh Pal on 12/02/17.
//  Copyright © 2016 Nilesh Pal. All rights reserved.
//

#ifndef Constant_h
#define Constant_h


/**************  Initial Objects Constant **************/
#define AppCntrlManager                     (((AppDelegate*)[UIApplication sharedApplication].delegate).appControllerManager)
#define AppDelegat                          ((AppDelegate*)[UIApplication sharedApplication].delegate)
/**************  Initial Objects Constant **************/

// Alert Messages

#define networkNotConnected @"Internet connection not found, Please make sure that you are connected with internet"
#define serverNotResponding @"Something went wrong, Please try after some time"
#define alertTitle @"OPGNY"


// Objects

#define SharedInstance [ApplicationManager sharedManagerInstance]

#define APP_DELEGATE ((AppDelegate *)[UIApplication sharedApplication].delegate)


// WebAPIs

//#define BASE_URL @"http://realplus.com/dev_opgny/Webservice/"
//#define BASE_URL @"https://opgny.com/Webservice/"
//#define REGISTER BASE_URL@""
#define GET_NEIGHBORHOOD BASE_URL_Application@"getNeighborhoodGroups" //GET

#define REQUEST_SUGGGESTION BASE_URL_Application@"suggestProperties" //POST

#define AGENT_DETAILS  BASE_URL_Application@"getMyAgent"

#define REQUEST_SCHEDULE BASE_URL_Application@"scheduleProperties" //POST
/*Histroy listing vc*/
//#define BASE_URL_Application @"https://opgny.com/staging/Webservice/"
//#define BASE_URL_Application @"http://realplus.com/dev_opgny/Webservice/"


//----------------------------BASE------------------------------
//#define BASE_URL_Application @"https://opgny.com/Webservice/"
//#define BASE_URL_Application @"http://dev.opgny.com/Webservice/"

#define BASE_URL_Application @"http://oxfordpg.com/Webservice/"

#define POST_LIKED BASE_URL_Application@"getLikedProperties"
#define POST_DISLIKED BASE_URL_Application@"getDislikedProperties"
#define POST_SUGGESTED BASE_URL_Application@"getSuggestedProperties"

#define POST_HISTORY_LIST_LIKE BASE_URL_Application@"getCustomerLikedProperties" //POST
#define POST_HISTORY_LIST_DIS_LIKE BASE_URL_Application@"getCustomerDislikedProperties"
#define POST_HISTORY_SUGESTED_LIKE BASE_URL_Application@"getSuggestedProperties"

#define POST_PROPERTY_LIST BASE_URL_Application@"getSearchResult"
#define POST_LIKE BASE_URL_Application@"likeDislikeProperty"
#define POST_SIGNIN_SIGNUP BASE_URL_Application@"commonSigninSignup"
#define POST_FORGOT_PASSWORD BASE_URL_Application@"forgotpassword"
#define FEED_BACK BASE_URL_Application@"feedback"
#define GET_AGENT_DETAIL BASE_URL_Application@"getRepresentative"
#define POST_UPDATE_PROFILE BASE_URL_Application@"updateProfile"
#define POST_UPDATE_PROFILE_IMAGE BASE_URL_Application@"updateProfilePicture"
#define CUSTOMER_INTEREST BASE_URL_Application@"likedDislikedSuggestedPropertiesRecords"

#define SCHEDULE_VIEWING BASE_URL_Application@"scheduleViewing"
#define ASSIGN_AGENT BASE_URL_Application@"assignAgent"
//----------------------------BASE------------------------------

//---------------------------- NSUSERDEFAULT ------------------------------

#define USER_PREF                           [NSUserDefaults standardUserDefaults]

#define USER_PREF_SET_OBJECT(value, key)    [USER_PREF setObject:value forKey:key]
#define USER_PREF_SET_VALUE(value, key)     [USER_PREF setValue::value forKey:key];
#define USER_PREF_GET_OBJECT(key)           [USER_PREF objectForKey:key]
#define USER_PREF_GET_VALUE(key)            [USER_PREF valueForKey:key]

#define USER_PREF_SET_BOOL(value, key)      [USER_PREF setBool:value forKey:key]
#define USER_PREF_GET_BOOL(key)             [USER_PREF boolForKey:key]

#define USER_PREF_SYNC                      [USER_PREF synchronize]

//---------------------------- NSUSERDEFAULT ------------------------------


#endif /* Constant_h */
