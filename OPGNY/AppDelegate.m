//
//  AppDelegate.m
//  OPGNY
//
//  Created by Nishat Ansari on 05/05/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.appControllerManager = [[AppControllerManager alloc] init];
    [self.appControllerManager initiateViewControllers];
    
    self.isFilterApply = NO;
    
    [GMSServices provideAPIKey:@"AIzaSyBXXdv3FEMHum3uRkoquIe_HSsk2kDY31Q"];
    [GIDSignIn sharedInstance].clientID = @"16452748260-avmsdhm1ckib73ceecab5v5mkftghldq.apps.googleusercontent.com";
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    
    [self checkUserPref];
    [self locationServices];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    
    // Add any custom logic here.
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation] || handled;
    
}


#pragma mark ---- For Location Manager ----

- (void)locationServices {
    
    self.locManager = [[CLLocationManager alloc] init];
    self.locManager.delegate = self;
    self.locManager.distanceFilter = kCLDistanceFilterNone;
    self.locManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locManager requestAlwaysAuthorization];
    [self.locManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    NSLog(@"Location Error %@",error.description);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
}

- (void)checkUserPref {
    
    if (![USER_PREF objectForKey:@"bedrooms"]) {
        [USER_PREF setObject:@"2" forKey:@"bedrooms"];
    }
    
    if (![USER_PREF objectForKey:@"bathrooms"]) {
        [USER_PREF setObject:@"2" forKey:@"bathrooms"];
    }
    
    if (![USER_PREF valueForKey:@"max_price"]) {
        [USER_PREF setValue:@"" forKey:@"max_price"];
    }
    
    if (![USER_PREF valueForKey:@"min_price"]) {
        [USER_PREF setValue:@"0" forKey:@"min_price"];
    }
    
    if (![USER_PREF objectForKey:@"area"]) {
        [USER_PREF setObject:@"" forKey:@"area"];
    }
    
    if (![USER_PREF valueForKey:@"sa_menu"]) {
        [USER_PREF setValue:@"" forKey:@"sa_menu"];
    }
    
    if (![USER_PREF objectForKey:@"neighborhood"]) {
        [USER_PREF setObject:@"" forKey:@"neighborhood"];
    }
    
    [USER_PREF synchronize];
}


@end
