//
//  AppDelegate.h
//  OPGNY
//
//  Created by Nishat Ansari on 05/05/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "AppControllerManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic)BOOL isFilterApply;
@property (strong, nonatomic)CLLocationManager *locManager;
@property (strong, nonatomic) AppControllerManager *appControllerManager;

@end

