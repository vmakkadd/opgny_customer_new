//
//  SizeSelectionViewController.h
//  OPGNY
//
//  Created by Nishat Ansari on 6/14/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SizeSelectionViewControllerDelegate <NSObject>
@optional
-(void)didGetSquareFeetSize:(NSString *)squareSize;
@end

@interface SizeSelectionViewController : UIViewController
{
   __weak IBOutlet UIView *viewOutlet;
   __weak IBOutlet UITextField *squareFeetTextField;

}
@property (retain, nonatomic) id <SizeSelectionViewControllerDelegate> delegate;
- (IBAction)confirmButtonAction:(UIButton *)sender;
- (IBAction)cancelButtonAcation:(UIButton *)sender;

@end
