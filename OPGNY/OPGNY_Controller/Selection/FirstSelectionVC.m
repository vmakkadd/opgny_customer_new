//
//  FirstSelectionVC.m
//  OPGNY
//
//  Created by Nishat Ansari on 11/05/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import "FirstSelectionVC.h"

@implementation FirstSelectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if (USER_PREF_GET_BOOL(@"isCommerical")) {
        [_btnLeasingRenting setTitle:@"LEASING" forState:UIControlStateNormal] ;
        self.lblTitle.text=@"COMMERCIAL";
    }
    else{
        [_btnLeasingRenting setTitle:@"RENTING" forState:UIControlStateNormal];
        self.lblTitle.text=@"RESIDENTIAL";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)clickLeasingOrBuying:(UIButton*)sender {
    
    if (sender.tag==1) {
        USER_PREF_SET_OBJECT(@"Sale", @"listing_type");
        [self.navigationController pushViewController:AppCntrlManager.homeVC animated:YES];
    }else{
        USER_PREF_SET_OBJECT(@"Rent", @"listing_type");
        [self.navigationController pushViewController:AppCntrlManager.locationSelectionViewController animated:YES];
    }
    USER_PREF_SYNC;
}

- (IBAction)clickPrevious:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
