//
//  LocationSelectionViewController.h
//  OPGNY
//
//  Created by Nishat Ansari on 05/06/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAMenuDropDown.h"
#import <RKTagsView/RKTagsView.h>
#import "RKCustomButton.h"
#import <CZPicker/CZPicker.h>
#import "SizeSelectionViewController.h"

@interface LocationSelectionViewController : UIViewController<SAMenuDropDownDelegate,RKTagsViewDelegate,CZPickerViewDataSource,CZPickerViewDelegate,UITextFieldDelegate,SizeSelectionViewControllerDelegate>
{
    SAMenuDropDown *dropLocation;
    NSArray *neighborhoodGroupNameArray;
    NSInteger selectedRegionIndex;
    NSMutableString *currentTextTag;
    NSMutableArray *selectIndexPath;
    
    __weak IBOutlet NSLayoutConstraint *mainViewHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *neighbourViewHeightConstraint;
    
    __weak IBOutlet UIView *tagSuperView;
    __weak IBOutlet UIView *containerView;
    
    __weak IBOutlet UILabel *titleLabel;
    
    __weak IBOutlet RKTagsView *tagsView;
    
    __weak IBOutlet UIScrollView *scrollView;
    
    __weak IBOutlet UIButton *selectButtonMainDropDow;
    __weak IBOutlet UIButton *sizeButtonOutlet;
}

- (IBAction)nextButtonAction:(UIButton *)sender;
- (IBAction)previourbuttonaction:(UIButton *)sender;
- (IBAction)selectButtonAction:(UIButton *)sender;
- (IBAction)neighborhood:(UIButton *)sender;
- (IBAction)sizeButtonAction:(UIButton *)sender;

@end
