//
//  SelectVC.m
//  OPGNY
//
//  Created by Nishat Ansari on 11/05/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import "SelectVC.h"
#import "FirstSelectionVC.h"

@implementation SelectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)clickCommercial:(id)sender {
    
    USER_PREF_SET_BOOL(YES, @"isCommerical");
    USER_PREF_SET_OBJECT(@"Commercial", @"property_type");
    USER_PREF_SYNC;
    [self.navigationController pushViewController:AppCntrlManager.firstSelectionVC animated:YES];
}

- (IBAction)clickResidential:(id)sender {
    
   USER_PREF_SET_BOOL(NO, @"isCommerical");
    USER_PREF_SET_OBJECT(@"Residential", @"property_type");
    USER_PREF_SYNC;
    [self.navigationController pushViewController:AppCntrlManager.firstSelectionVC animated:YES];
}


@end
