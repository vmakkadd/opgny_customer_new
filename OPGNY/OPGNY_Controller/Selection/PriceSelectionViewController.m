//
//  PriceSelectionViewController.m
//  OPGNY
//
//  Created by Nishat Ansari on 6/19/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import "PriceSelectionViewController.h"

@implementation PriceSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if ([USER_PREF_GET_VALUE(@"max_price")  length]) {
        maxPriceTextfield.text = [SharedInstance setAmountInCommaSeperated:USER_PREF_GET_VALUE(@"max_price")];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    NSLog(@"maxPriceTextfield.text.length ....%lu",maxPriceTextfield.text.length);
    
    if (maxPriceTextfield.text.length > 0) {
        USER_PREF_SET_OBJECT([maxPriceTextfield.text stringByReplacingOccurrencesOfString:@"," withString:@""], @"max_price");
        USER_PREF_SET_OBJECT(@"0", @"min_price");
    }
    else{
        USER_PREF_SET_OBJECT(@"0", @"max_price");
        USER_PREF_SET_OBJECT(@"0", @"min_price");
    }
    USER_PREF_SYNC;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)confirmButtonAction:(UIButton *)sender {
    [self cancelButtonAction:nil];
}

- (IBAction)cancelButtonAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
