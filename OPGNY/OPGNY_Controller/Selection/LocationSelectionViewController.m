//
//  LocationSelectionViewController.m
//  OPGNY
//
//  Created by Nishat Ansari on 05/06/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import "LocationSelectionViewController.h"


@implementation LocationSelectionViewController
{
    NSAttributedString *neighborhoodAttributedString;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mainViewHeightConstraint.constant = 700 + (tagsView.frame.size.height *2);
    
    dropLocation=[[SAMenuDropDown alloc] initWithWithSource:selectButtonMainDropDow menuHeight:250 itemNames:nil itemImagesName:nil itemSubtitles:nil];
    [dropLocation bringSubviewToFront:containerView];
    [dropLocation setRowHeight:44];
    dropLocation.delegate = self ;
    
    selectIndexPath = [NSMutableArray new];
    tagSuperView.layer.borderColor = [UIColor whiteColor].CGColor;
    neighborhoodAttributedString = [[NSAttributedString alloc]initWithString:@"Neighborhood" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Roboto-Medium" size:14]}];
    tagsView.textField.attributedPlaceholder = neighborhoodAttributedString;
    tagsView.textField.returnKeyType = UIReturnKeyDone;
    tagsView.textField.delegate = self;
    tagsView.tintColor = [UIColor whiteColor];
    tagsView.allowsMultipleSelection = true;
    tagsView.selectable = true;
    tagsView.editable = true;
    tagsView.delegate = self;
    tagsView.textField.alpha = 0;
    
    [self callAPI];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if (!USER_PREF_GET_BOOL(@"isCommerical")){
        titleLabel.text=@"RESIDENTIAL";
    }else
        titleLabel.text=@"LEASING";
    
    [dropLocation setUpItemDataSourceWithNames:neighborhoodGroupNameArray subtitles:nil imageNames:nil];
    
    [tagsView.textField setFrame:CGRectMake(tagsView.textField.frame.origin.x, tagsView.textField.frame.origin.y+5, tagsView.textField.frame.size.width, tagsView.textField.frame.size.height)];
    tagsView.textField.alpha = 1;
    
    [sizeButtonOutlet setTitle:USER_PREF_GET_OBJECT(@"area") forState:UIControlStateNormal];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:YES];
    
    if (tagsView.tags.count > 0) {
        USER_PREF_SET_OBJECT(currentTextTag, @"neighborhood");
        tagsView.textField.placeholder = @"";
    }
    else {
        USER_PREF_SET_OBJECT(@"", @"neighborhood");
    }
    USER_PREF_SYNC;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Buttons Action

- (IBAction)nextButtonAction:(UIButton *)sender {
    
    USER_PREF_SET_OBJECT(neighborhoodGroupNameArray[selectedRegionIndex][@"neighborhood_list"], @"locArr");
   
    if (tagsView.tags.count > 0) {
        USER_PREF_SET_OBJECT(currentTextTag ,@"neighborhood");
    }
    else {
        USER_PREF_SET_OBJECT(@"" ,@"neighborhood");
    }
    [USER_PREF synchronize];


    [self.navigationController pushViewController:AppCntrlManager.budgetSectionViewController animated:YES];
}

- (IBAction)previourbuttonaction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)selectButtonAction:(UIButton *)sender {
    if (neighborhoodGroupNameArray.count > 0) {
        if ([dropLocation isOpen]) {
            [dropLocation hideSADropDownMenu];
        }
        else{
            [dropLocation setUpItemDataSourceWithNames:neighborhoodGroupNameArray subtitles:nil imageNames:nil];
            [dropLocation self_showSADropDownMenuWithAnimation:kSAMenuDropAnimationDirectionBottom];
        }
    }
}

- (IBAction)neighborhood:(UIButton *)sender {
    if (selectButtonMainDropDow.currentTitle.length > 0) {
        if ([neighborhoodGroupNameArray[selectedRegionIndex][@"neighborhood_list"] count] > 0) {
            NSString *titleStr = selectButtonMainDropDow.currentTitle;
            
            NSArray *tempArr = [currentTextTag componentsSeparatedByString:@","];
            
            selectIndexPath = [NSMutableArray new];
            
            for (NSString *strCheck in neighborhoodGroupNameArray[selectedRegionIndex][@"neighborhood_list"]) {
                if ([tempArr containsObject:strCheck]) {
                    NSIndexPath *index = [NSIndexPath indexPathForRow:[neighborhoodGroupNameArray[selectedRegionIndex][@"neighborhood_list"] indexOfObject:strCheck] inSection:0];
                    [selectIndexPath addObject:index];
                    
                }
            }
            [self showPickerWithMultipleSelection:titleStr];
            
        }else
            [SharedInstance showAlertWithMessage:@"Neighborhood does not found for selected region. Please try again with another region."];
    }
    else {
        [SharedInstance showAlertWithMessage:@"Please select region"];
    }
}

- (IBAction)sizeButtonAction:(UIButton *)sender {
    AppCntrlManager.sizeSelectionViewController.delegate = self;
    [self presentViewController:AppCntrlManager.sizeSelectionViewController animated:YES completion:nil];
}

#pragma mark -
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark - RKTagsViewDelegate

- (UIButton *)tagsView:(RKTagsView *)tagsView buttonForTagAtIndex:(NSInteger)index {
    RKCustomButton *customButton = [RKCustomButton buttonWithType:UIButtonTypeSystem];
    customButton.titleLabel.font = tagsView.font;
    [customButton setTitle:[NSString stringWithFormat:@"%@,", tagsView.tags[index]] forState:UIControlStateNormal];
    [customButton runBubbleAnimation];
    return customButton;
}

#pragma mark -
#pragma mark - Show and Hide SAMenuDrop

- (void)saDropMenu:(SAMenuDropDown *)menuSender didClickedAtIndex:(NSInteger)buttonIndex
{
    [selectButtonMainDropDow setTitle:[[neighborhoodGroupNameArray objectAtIndex:buttonIndex] objectForKey:@"neighborhood_group_name"] forState:UIControlStateNormal];
    selectedRegionIndex = buttonIndex;
    
    [USER_PREF setValue:[[neighborhoodGroupNameArray objectAtIndex:buttonIndex] objectForKey:@"neighborhood_group_name"] forKey:@"sa_menu"];
    USER_PREF_SET_OBJECT(@"", @"neighborhood");
    USER_PREF_SYNC;
    
    [tagsView removeAllTags ];
    [tagsView reloadInputViews];
    [dropLocation hideSADropDownMenu];
    
    neighborhoodAttributedString = [[NSAttributedString alloc]initWithString:@"Neighborhood" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Roboto-Medium" size:14]}];
    tagsView.textField.attributedPlaceholder = neighborhoodAttributedString;

    mainViewHeightConstraint.constant = 700 + (tagsView.frame.size.height *2);
}

#pragma mark -
#pragma mark - CZPickerView DataSource & Delegate

- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row{
    
    NSAttributedString *att = [[NSAttributedString alloc]
                               initWithString:neighborhoodGroupNameArray[selectedRegionIndex][@"neighborhood_list"][row]
                               attributes:@{
                                            //NSFontAttributeName:[UIFont systemFontOfSize:18.0f]
                                            }];
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row{
    return neighborhoodGroupNameArray[selectedRegionIndex][@"neighborhood_list"][row];
}


- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView{
    return [neighborhoodGroupNameArray[selectedRegionIndex][@"neighborhood_list"] count];
}

-(void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows{

    [tagsView.textField setFrame:CGRectMake(tagsView.textField.frame.origin.x, tagsView.textField.frame.origin.y+5, tagsView.textField.frame.size.width, tagsView.textField.frame.size.height)];
    
    currentTextTag = [NSMutableString new];
    for (int i = 0;i<rows.count;i++){
        //for(NSNumber *n in rows){
        NSInteger row = [rows[i] integerValue];
        NSLog(@"%@ is chosen!", neighborhoodGroupNameArray[selectedRegionIndex][@"neighborhood_list"][row]);
        
        if (i == rows.count-1) {
            [currentTextTag appendString:neighborhoodGroupNameArray[selectedRegionIndex][@"neighborhood_list"][row]];
        }
        else {
            [currentTextTag appendFormat:@"%@,",neighborhoodGroupNameArray[selectedRegionIndex][@"neighborhood_list"][row]];
        }
    }
    
    if (rows.count == 0) {
        
        selectIndexPath = [NSMutableArray new];
    }
    
    [tagsView removeAllTags ];
    
    if (currentTextTag.length > 0) {
        
        NSArray *tempArr = [currentTextTag componentsSeparatedByString:@","];
        for (NSString *word in tempArr) {
            if (word.length > 0) {
                [tagsView addTag:word];
            }
        }
        //        [_lblPlaceHolder setHidden:true];
        tagsView.textField.placeholder = @"";
        
    }
    else {
        tagsView.textField.attributedPlaceholder = neighborhoodAttributedString;
    }
    
    mainViewHeightConstraint.constant = 700 + (tagsView.frame.size.height *2);
    
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    NSLog(@"Canceled.");
}


- (void)showPickerWithMultipleSelection:(NSString *)Title {
    
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:Title cancelButtonTitle:@"Cancel" confirmButtonTitle:@"Confirm"];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = YES;
    picker.selectedIndexPaths = selectIndexPath;
    picker.headerTitleColor = [UIColor darkGrayColor];
    picker.headerBackgroundColor = [UIColor whiteColor];
    
    [picker show];
}

#pragma mark -
#pragma mark - SizeSquareViewControllerDelegate Methods

-(void)didGetSquareFeetSize:(NSString *)squareSize
{
    [sizeButtonOutlet setTitle:squareSize forState:UIControlStateNormal];
}

#pragma mark -
#pragma mark - API Server Calling

- (void)callAPI {
    
    [SharedInstance callGETWebService:GET_NEIGHBORHOOD andView:self.view andgetData:^(NSDictionary *data, NSError *error) {
        if (data) {
            NSLog(@"data %@",data);
            if (data[@"result"]) {
                neighborhoodGroupNameArray = data[@"result"];
                
                if (neighborhoodGroupNameArray.count > 0) {
                    if ([USER_PREF_GET_VALUE(@"sa_menu") length]>0) {
                        [selectButtonMainDropDow setTitle:USER_PREF_GET_VALUE(@"sa_menu") forState:UIControlStateNormal];
                        
                        for (int i = 0; i< neighborhoodGroupNameArray.count; i++) {
                            if ([neighborhoodGroupNameArray[i][@"neighborhood_group_name"] isEqualToString:USER_PREF_GET_VALUE(@"sa_menu")]) {
                                selectedRegionIndex = i;
                                break;
                            }
                            
                        }
                        
                        currentTextTag = [[NSMutableString alloc]initWithString:USER_PREF_GET_OBJECT(@"neighborhood")];
                        
                        if (currentTextTag.length > 0) {
                            
                            NSArray *tempArr = [currentTextTag componentsSeparatedByString:@","];
                            for (NSString *word in tempArr) {
                                if (word.length > 0) {
                                    [tagsView addTag:word];
                                }
                            }
                            tagsView.textField.placeholder = @"";
                        }
                        else {
                            tagsView.textField.attributedPlaceholder = neighborhoodAttributedString;
                        }
                        
                    }
                    else {
                        
                        [USER_PREF setValue:neighborhoodGroupNameArray[0][@"neighborhood_group_name"] forKey:@"sa_menu"];
                        
                        [selectButtonMainDropDow setTitle:neighborhoodGroupNameArray[0][@"neighborhood_group_name"] forState:UIControlStateNormal];
                        selectedRegionIndex = 0;
                    }
                }
                else {
                    [selectButtonMainDropDow setTitle:@"" forState:UIControlStateNormal];
                }
            }
            else {
                [SharedInstance showAlertWithMessage:data[@"message"]];
            }
        }
        else {
            [SharedInstance showAlertWithMessage:@"Something went wrong. Please try again!"];
        }
    }];
}


@end
