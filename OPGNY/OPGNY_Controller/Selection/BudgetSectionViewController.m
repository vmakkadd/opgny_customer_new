//
//  BudgetSectionViewController.m
//  OPGNY
//
//  Created by Nishat Ansari on 6/18/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import "BudgetSectionViewController.h"

@implementation BudgetSectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSAttributedString *placeholderAttributedString = [[NSAttributedString alloc]initWithString:@"Enter your budget." attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Roboto-Medium" size:14]}];
    budgetTextField.attributedPlaceholder = placeholderAttributedString;
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [residentialView setHidden:YES];
    //containerViewheightConstraint.constant = 548;
    titleLabel.text=@"LEASING";
    
    if (!USER_PREF_GET_BOOL(@"isCommerical")){
        titleLabel.text=@"RESIDENTIAL";
        [residentialView setHidden:NO];
        numberOfBedroomLabel.text = USER_PREF_GET_OBJECT(@"bedrooms");
        numberOfBathroomLabel.text = USER_PREF_GET_OBJECT(@"bathrooms");
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [self updateValueOnDefaults];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)bedroomButtonActions:(UIButton *)sender {
    NSInteger bedroomCount = [numberOfBedroomLabel.text integerValue];
    
    if (sender.tag==0) {
        if (bedroomCount<=4) {
            bedroomCount++;
        }
    }else{
        if (bedroomCount>=1) {
            bedroomCount--;
        }
    }
    
    numberOfBedroomLabel.text = [NSString stringWithFormat:@"%ld",(long)bedroomCount];
}

- (IBAction)bathroomButtonActions:(UIButton *)sender {
    NSInteger bathroomCount = [numberOfBathroomLabel.text integerValue];
    
    if (sender.tag==0) {
        if (bathroomCount<=4) {
            bathroomCount++;
        }
    }else{
        if (bathroomCount>=1) {
            bathroomCount--;
        }
    }
    numberOfBathroomLabel.text = [NSString stringWithFormat:@"%ld",(long)bathroomCount];
}

- (IBAction)nextButtonAction:(UIButton *)sender
{
    [self updateValueOnDefaults];
    [self callWebSerivce];
}

- (IBAction)previourbuttonaction:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - Call Search Api

-(void)callWebSerivce
{
    NSMutableDictionary *dictRequest = [[NSMutableDictionary alloc]init];
    
    [dictRequest setValue:USER_PREF_GET_VALUE(@"property_type") forKey:@"property_type"];
    [dictRequest setValue:USER_PREF_GET_VALUE(@"listing_type") forKey:@"listing_type"];
    [dictRequest setValue:USER_PREF_GET_VALUE(@"max_price") forKey:@"max_price"];
    [dictRequest setValue:USER_PREF_GET_VALUE(@"min_price") forKey:@"min_price"];
    [dictRequest setValue:USER_PREF_GET_VALUE(@"neighborhood") forKey:@"neighborhood"];
    
    
    if(USER_PREF_GET_VALUE(@"customer_id")!= nil)
    {
        [dictRequest setValue:USER_PREF_GET_VALUE(@"customer_id") forKey:@"user_id"];
    }
    else{
        [dictRequest setValue:@"-1" forKey:@"user_id"];
    }
    
    if(USER_PREF_GET_BOOL(@"isCommerical")) {
        [dictRequest setValue:USER_PREF_GET_VALUE(@"area") forKey:@"area"];
    }
    else{
        
        [dictRequest setValue:USER_PREF_GET_VALUE(@"bedrooms") forKey:@"bedrooms"];
        [dictRequest setValue:USER_PREF_GET_VALUE(@"bathrooms") forKey:@"bathrooms"];
    }
    
    [dictRequest setValue:@"10" forKey:@"start_index"];
    
    NSString *strURL=[NSString stringWithFormat:@"%@",POST_PROPERTY_LIST];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSDictionary *dict = [[NSDictionary alloc]initWithDictionary:dictRequest];
    
    [SharedInstance callPOSTWebServiceFormData:dict andURL:strURL andView:self.view  andgetData:^(NSDictionary *data, NSError *error) {
        if (data) {
            
            [MBProgressHUD hideHUDForView:self.view animated:true];
            NSLog(@"data %@",data);
            if ([data[@"status"] isEqualToString:@"success"]) {
                //AppCntrlManager.homeVC.arrayList = [NSMutableArray arrayWithArray:data[@"result"]];
                APP_DELEGATE.isFilterApply = YES;
                [self.navigationController pushViewController:AppCntrlManager.homeVC animated:true];
            }
        }
        else {
            [MBProgressHUD hideHUDForView:self.view animated:true];
            [SharedInstance showAlertWithMessage:@"Something went wrong. Please try again!"];
        }
    }];
    
}

-(void)updateValueOnDefaults
{
    USER_PREF_SET_OBJECT(numberOfBedroomLabel.text, @"bedrooms");
    USER_PREF_SET_OBJECT(numberOfBathroomLabel.text, @"bathrooms");
    
    if (budgetTextField.text.length > 0) {
        USER_PREF_SET_OBJECT([budgetTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""], @"max_price");
        USER_PREF_SET_OBJECT(@"0", @"min_price");
    }
    else{
        USER_PREF_SET_OBJECT(@"0", @"max_price");
        USER_PREF_SET_OBJECT(@"0", @"min_price");
    }
    USER_PREF_SYNC;
}

@end
