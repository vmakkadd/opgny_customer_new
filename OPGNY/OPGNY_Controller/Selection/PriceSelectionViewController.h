//
//  PriceSelectionViewController.h
//  OPGNY
//
//  Created by Nishat Ansari on 6/19/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PriceSelectionViewController : UIViewController
{
    __weak IBOutlet UITextField *minPriceTextfield;
    __weak IBOutlet UITextField *maxPriceTextfield;
}

- (IBAction)confirmButtonAction:(UIButton *)sender;
- (IBAction)cancelButtonAction:(UIButton *)sender;

@end
