//
//  SizeSelectionViewController.m
//  OPGNY
//
//  Created by Nishat Ansari on 6/14/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import "SizeSelectionViewController.h"

@implementation SizeSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    viewOutlet.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6f];
    viewOutlet.opaque = NO;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, squareFeetTextField.frame.size.height)];
    squareFeetTextField.leftView = paddingView;
    squareFeetTextField.leftViewMode = UITextFieldViewModeAlways;
    
    squareFeetTextField.layer.borderColor = [UIColor colorWithRed:181.0f/255.0f green:198.0f/255.0f blue:220.0f/255.0f alpha:1].CGColor;
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    squareFeetTextField.text = USER_PREF_GET_OBJECT(@"area");
}

-(void)viewWillDisappear:(BOOL)animated {
    if (squareFeetTextField.text.length > 0) {
        USER_PREF_SET_OBJECT(squareFeetTextField.text, @"area");
    }
    else{
        USER_PREF_SET_OBJECT(@"", @"area");
    }
    USER_PREF_SYNC;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)confirmButtonAction:(UIButton *)sender {
    
    if (![squareFeetTextField.text isEqualToString:@""])
    {
        if ([self.delegate respondsToSelector:@selector(didGetSquareFeetSize:)]) {
            [self.delegate didGetSquareFeetSize:squareFeetTextField.text];
        }
        [self cancelButtonAcation:nil];
    }else
        [SharedInstance showAlertWithMessage:@"Please enter size."];
}

- (IBAction)cancelButtonAcation:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
