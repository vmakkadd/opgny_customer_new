//
//  SelectVC.h
//  OPGNY
//
//  Created by Nishat Ansari on 11/05/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnCommertial;
@property (weak, nonatomic) IBOutlet UIButton *btnResidential;

@end
