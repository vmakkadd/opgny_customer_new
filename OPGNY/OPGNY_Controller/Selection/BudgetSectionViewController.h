//
//  BudgetSectionViewController.h
//  OPGNY
//
//  Created by Nishat Ansari on 6/18/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BudgetSectionViewController : UIViewController
{
    __weak IBOutlet UILabel *numberOfBedroomLabel;
    __weak IBOutlet UILabel *numberOfBathroomLabel;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet NSLayoutConstraint *containerViewheightConstraint;
    __weak IBOutlet UIView *residentialView;
    __weak IBOutlet UITextField *budgetTextField;
}


- (IBAction)bedroomButtonActions:(UIButton *)sender;
- (IBAction)bathroomButtonActions:(UIButton *)sender;
- (IBAction)nextButtonAction:(UIButton *)sender;
- (IBAction)previourbuttonaction:(UIButton *)sender;

@end
