//
//  FirstSelectionVC.h
//  OPGNY
//
//  Created by Nishat Ansari on 11/05/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstSelectionVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnLeasingRenting;

- (IBAction)clickLeasingOrBuying:(UIButton*)sender;
@end
