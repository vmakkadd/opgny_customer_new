//
//  MyProfileVC.m
//  OPGNY
//
//  Created by Nishat Ansari on 10/05/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import "MyProfileVC.h"
#import "KYDrawerController.h"

@interface MyProfileVC ()

@end

@implementation MyProfileVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _imgProfile.layer.cornerRadius = _imgProfile.frame.size.width / 2;
    _imgProfile.clipsToBounds = YES;
    _imgProfile.layer.borderWidth=5;
    _imgProfile.layer.borderColor=[UIColor colorWithRed:78/255.0 green:90/255.0 blue:247/255.0 alpha:1].CGColor;
    
    _btnSchedule.layer.cornerRadius = _btnSchedule.frame.size.height / 2;
    _btnSchedule.clipsToBounds = YES;
    
    _btnRequest.layer.cornerRadius = _btnRequest.frame.size.height / 2;
    _btnRequest.clipsToBounds = YES;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickBack:(id)sender {
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
}


@end
