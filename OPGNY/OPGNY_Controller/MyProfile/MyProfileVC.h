//
//  MyProfileVC.h
//  OPGNY
//
//  Created by Nishat Ansari on 10/05/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnRequest;

@property (weak, nonatomic) IBOutlet UIButton *btnSchedule;
@end
