//
//  ViewController.h
//  OPGNY
//
//  Created by Nilesh Pal on 12/02/17.
//  Copyright © 2017 Nilesh Pal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVC : UIViewController<GIDSignInDelegate,GIDSignInUIDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnAutoLogin;

- (IBAction)action_Google:(UIButton *)sender;
- (IBAction)action_FB:(UIButton *)sender;
- (IBAction)btnSignup:(id)sender;
- (IBAction)btnLoginAction:(UIButton *)sender;
- (IBAction)btnAutoLoginAction:(UIButton *)sender;
- (IBAction)btnBackAction:(id)sender;
- (IBAction)btnForgotPasswordAction:(id)sender;

@end

