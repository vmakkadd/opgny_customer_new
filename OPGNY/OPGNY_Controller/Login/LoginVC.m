//
//  ViewController.m
//  OPGNY
//
//  Created by Nilesh Pal on 12/02/17.
//  Copyright © 2017 Nilesh Pal. All rights reserved.
//

#import "LoginVC.h"
//#import "RegisterVC.h"
//#import "StepFirstVC.h"
//#import "ForgotPwdVC.h"
#import "KYDrawerController.h"

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self updateLocalPref];
    
    /*add view */
    /*UIView *leftViewUserName = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 40)];
    _txtEmailAddress.leftView = leftViewUserName;
    _txtEmailAddress.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *leftViewPassword = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 40)];
    _txtPassword.leftView = leftViewPassword;
    _txtPassword.leftViewMode = UITextFieldViewModeAlways;*/
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Button Actions

- (IBAction)action_Google:(UIButton *)sender {
    
    if ([SharedInstance isNetworkConnected]) {
        [[GIDSignIn sharedInstance] signIn];
    }
    else {
        [SharedInstance showAlertWithMessage:networkNotConnected];
    }

}

- (IBAction)action_FB:(UIButton *)sender {
    
    if ([SharedInstance isNetworkConnected]) {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        
        [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error) {
                NSLog(@"Process error");
            } else if (result.isCancelled) {
                NSLog(@"Cancelled");
            } else {
                NSLog(@"Logged in");
                if ([result.grantedPermissions containsObject:@"email"]) {
                    if ([FBSDKAccessToken currentAccessToken]) {
                        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                        [parameters setValue:@"id,name,email,first_name, last_name,picture.type(large)" forKey:@"fields"];
                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                             
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             if (!error) {
                                 NSLog(@"FB Result %@",result);
                                 
                                 NSString *strProfilePicture = [[[result objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
                                 
                                 NSDictionary *dict = @{@"facebook_id":[result objectForKey:@"id"],@"google_id":@"0",@"first_name":[result objectForKey:@"first_name"],@"email":[result objectForKey:@"email"],@"last_name":[result objectForKey:@"last_name"],@"profile_picture":strProfilePicture};
                                 USER_PREF_SET_OBJECT(strProfilePicture, @"profile_picture_url");
                                 USER_PREF_SYNC;
                                 [self btnFacebookAction:dict];
                                 
                             }
                             else {
                                 NSLog(@"Error :%@",error.description);
                             }
                         }];
                    }
                }
            }
        }];
    }
    else {
        [SharedInstance showAlertWithMessage:networkNotConnected];
    }
    
    
}
- (IBAction)btnSignup:(id)sender {
   /* RegisterVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
    [self.navigationController pushViewController:vc animated:true];*/
}

/*normal login*/

- (IBAction)btnLoginAction:(UIButton *)sender
{
    if ([SharedInstance isNetworkConnected]) {
        if ([self isValidate]) {
            NSString *strURL=[NSString stringWithFormat:@"%@",POST_SIGNIN_SIGNUP];
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            NSDictionary *dict = @{@"email":_txtEmailAddress.text,@"password":_txtPassword.text};
            
            [SharedInstance callPOSTWebServiceFormData:dict andURL:strURL andView:self.view  andgetData:^(NSDictionary *data, NSError *error) {
                if (data) {
                    [MBProgressHUD hideHUDForView:self.view animated:true];
                    NSLog(@"data %@",data);
                    if ([data[@"status"] isEqualToString:@"success"]) {
                        
                        USER_PREF_SET_OBJECT(data[@"result"][0], @"user-details");
                        USER_PREF_SET_OBJECT(data[@"result"][0][@"id"], @"customer_id");
                        USER_PREF_SYNC;
                
                        NSLog(@"%@-%@",[USER_PREF objectForKey:@"customer_id"],[USER_PREF objectForKey:@"user-details"]);
                        if (self.btnAutoLogin.isSelected) {
                            [USER_PREF setBool:YES forKey:@"isLoggedIn"];
                        }
                        else{
                            [USER_PREF setBool:NO forKey:@"isLoggedIn"];
                        }
                        
                        NSArray *dictResult = data[@"result"][0][@"representative"][@"result"];
                        
                        if ([dictResult isKindOfClass:[NSDictionary class]]) {
                            [USER_PREF setBool:true forKey:@"is_agent_detail"];
                            [USER_PREF setValue:dictResult  forKey:@"agent_detail"];
                        }
                        
                        else{
                            [USER_PREF setBool:false forKey:@"is_agent_detail"];
                        }
                        NSLog(@"%@",[[data valueForKey:@"result"] valueForKey:@"profile_picture_url"]);
                        
                        NSString *str = [[[data valueForKey:@"result"] objectAtIndex:0] valueForKey:@"profile_picture_url"];
                        
                        [USER_PREF setValue:str forKey:@"profile_picture_url"];
                        [USER_PREF synchronize];
                        
                        [self btnBackAction:nil];
                        
                        //                        SelectVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectVC"];
                        //                        [self.navigationController pushViewController:vc animated:true];
                    }
                    else {
                        [MBProgressHUD hideHUDForView:self.view animated:true];
                        [SharedInstance showAlertWithMessage:data[@"result"]];
                    }
                }
                else {
                    [MBProgressHUD hideHUDForView:self.view animated:true];
                    [SharedInstance showAlertWithMessage:@"Something went wrong. Please try again!"];
                }
            }];
            
        }
    }
    else{
        [SharedInstance showAlertWithMessage:networkNotConnected];
        
    }
    
}

- (IBAction)btnForgotPasswordAction:(id)sender {
    
    /*ForgotPwdVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPwdVC"];
     [self.navigationController pushViewController:vc animated:true];*/
}

- (IBAction)btnAutoLoginAction:(UIButton *)sender {
    
    if ([sender isSelected]) {
        [USER_PREF setBool:false forKey:@"isLogin"];
        [sender setSelected:false];
    }
    else{
        [USER_PREF setBool:true forKey:@"isLogin"];
        [sender setSelected:true];
    }
}
- (IBAction)btnBackAction:(id)sender {
    KYDrawerController *drawer = (KYDrawerController *) [self.navigationController parentViewController];
    HomeVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
    APP_DELEGATE.isFilterApply = YES;
    //Closing drawer whene new controller appeares
    
    drawer.mainViewController=navController;
    [drawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:true];
}


#pragma mark -
#pragma mark - Google SignIn Delegate

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
}
// Present a view that prompts the user to sign in with Google

- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    [self presentViewController:viewController animated:YES completion:nil];
}
// Dismiss the "Sign in with Google" view

- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
//completed sign In

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    //user signed in
    //get user data in "user" (GIDGoogleUser object)
    if (user) {
        NSString *userId = user.userID;
        NSString *idToken = user.authentication.idToken;
        NSString *fullName = user.profile.name;
        NSString *givenName = user.profile.givenName;
        NSString *familyName = user.profile.familyName;
        NSString *email = user.profile.email;
        NSString *imgURL;
        if (user.profile.hasImage) {
            imgURL = [user.profile imageURLWithDimension:200].absoluteString;
        }
        else{
            imgURL = @"";
        }
        
//        [signIn signOut];
        
        NSDictionary *dict = @{@"facebook_id":@"0",@"google_id":userId,@"first_name":fullName,@"email":email,@"last_name":familyName,@"profile_picture":imgURL};
        
        [USER_PREF setValue:imgURL forKey:@"profile_picture_url"];
        [USER_PREF synchronize];
        [self btnGmailLoginAction:dict];

    }
    
}

#pragma mark -
#pragma mark - Login with Facebook

/*normal login*/
-(void)btnFacebookAction:(NSDictionary *)dict
{
    if ([SharedInstance isNetworkConnected]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:true];
        NSString *strURL=[NSString stringWithFormat:@"%@",POST_SIGNIN_SIGNUP];
        [SharedInstance callPOSTWebServiceFormData:dict andURL:strURL andView:self.view  andgetData:^(NSDictionary *data, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:true];
            if (data) {
                [MBProgressHUD hideHUDForView:self.view animated:true];
                NSLog(@"data %@",data);
                if ([data[@"status"] isEqualToString:@"success"]) {
                    
                    [USER_PREF setObject:data[@"result"][0] forKey:@"user-details"];
                    [USER_PREF setValue:data[@"result"][0][@"id"] forKey:@"customer_id"];
                    NSLog(@"%@-%@",[USER_PREF objectForKey:@"customer_id"],[USER_PREF objectForKey:@"user-details"]);
                    [USER_PREF setBool:YES forKey:@"isLoggedIn"];
                    [USER_PREF synchronize];
                    SelectVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectVC"];
                    [self.navigationController pushViewController:vc animated:true];
                }
                else {
                    [MBProgressHUD hideHUDForView:self.view animated:true];
                    [SharedInstance showAlertWithMessage:data[@"result"]];
                }
            }
            else {
                [MBProgressHUD hideHUDForView:self.view animated:true];
                [SharedInstance showAlertWithMessage:@"Something went wrong. Please try again!"];
            }
        }];

    }
    else{
        [SharedInstance showAlertWithMessage:networkNotConnected];
        
    }
    
    
}

#pragma mark -
#pragma mark - Login with Gmail

-(void)btnGmailLoginAction:(NSDictionary *)dict
{
    if ([SharedInstance isNetworkConnected]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:true];
        NSString *strURL=[NSString stringWithFormat:@"%@",POST_SIGNIN_SIGNUP];
        [SharedInstance callPOSTWebServiceFormData:dict andURL:strURL andView:self.view  andgetData:^(NSDictionary *data, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:true];
            if (data) {
                [MBProgressHUD hideHUDForView:self.view animated:true];
                NSLog(@"data %@",data);
                if ([data[@"status"] isEqualToString:@"success"]) {
                    
                    [USER_PREF setObject:data[@"result"][0] forKey:@"user-details"];
                    [USER_PREF setValue:data[@"result"][0][@"id"] forKey:@"customer_id"];
                    NSLog(@"%@-%@",[USER_PREF objectForKey:@"customer_id"],[USER_PREF objectForKey:@"user-details"]);
                    [USER_PREF setBool:YES forKey:@"isLoggedIn"];
                    [USER_PREF synchronize];
                    SelectVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectVC"];
                    [self.navigationController pushViewController:vc animated:true];
                }
                else {
                    [MBProgressHUD hideHUDForView:self.view animated:true];
                    [SharedInstance showAlertWithMessage:data[@"result"]];
                }
            }
            else {
                [MBProgressHUD hideHUDForView:self.view animated:true];
                [SharedInstance showAlertWithMessage:@"Something went wrong. Please try again!"];
            }
        }];

    }
    else{
        [SharedInstance showAlertWithMessage:networkNotConnected];
        
    }
    
}
#pragma mark -
#pragma mark - Self Customize Methods


-(BOOL)isValidate
{
    NSString *strEmail = [NSString stringWithFormat:@"%@",_txtEmailAddress.text];
    strEmail = [SharedInstance removeWhiteSpace:strEmail];
    
    NSString *strPassword = [NSString stringWithFormat:@"%@",_txtPassword.text];
    strPassword = [SharedInstance removeWhiteSpace:strPassword];
    
    if (strEmail.length == 0) {
        
        [SharedInstance showAlertWithMessage:@"Please enter email or mobile number."];
        return NO;
    }
    else if (![SharedInstance emailAddressIsValid:strEmail] || ![SharedInstance mobileNumberValidator:strEmail])
    {
        [SharedInstance showAlertWithMessage:@"Please enter valid email or mobile number."];
        return NO;
    }
    else if (strPassword.length == 0)
    {
        [SharedInstance showAlertWithMessage:@"Please enter password."];
        return NO;
    }
    return true;
}


- (void)updateLocalPref {
    [USER_PREF setObject:@"2" forKey:@"bedrooms"];
    [USER_PREF setObject:@"2" forKey:@"bathrooms"];
    [USER_PREF setValue:@"" forKey:@"max_price"];
    [USER_PREF setValue:@"0" forKey:@"min_price"];
    [USER_PREF setObject:@"" forKey:@"area"];
    [USER_PREF setValue:@"" forKey:@"sa_menu"];
    [USER_PREF setObject:@"" forKey:@"neighborhood"];
    [USER_PREF synchronize];
}

@end
