//
//  InitialScreenVC.m
//  OPGNY
//
//  Created by Dheerendra chaturvedi on 12/02/17.
//  Copyright © 2017 Dheerendra chaturvedi. All rights reserved.
//
#import "SelectVC.h"
#import "InitialScreenVC.h"
#import "KYDrawerController.h"
@interface InitialScreenVC ()<UIScrollViewDelegate>
{
    NSInteger intPagintaion;
    NSArray *colorArray;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic, assign) CGFloat lastContentOffset;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;

@property (weak, nonatomic) IBOutlet UIButton *btnSwipe;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constSkip;

@end

@implementation InitialScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = true;
    intPagintaion = 0;
    
    colorArray = [[NSArray alloc]initWithObjects:[UIColor colorWithRed:34.0f/255.0f green:44.0f/255.0f blue:112.0f/255.0f alpha:1],[UIColor colorWithRed:47.0f/255.0f green:103.0f/255.0f blue:133.0f/255.0f alpha:1],[UIColor colorWithRed:50.0f/255.0f green:47.0f/255.0f blue:182.0f/255.0f alpha:1],[UIColor colorWithRed:11.0f/255.0f green:82.0f/255.0f blue:255.0f/255.0f alpha:1], nil];
    self.scrollView.backgroundColor = [UIColor colorWithRed:34.0f/255.0f green:44.0f/255.0f blue:112.0f/255.0f alpha:1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 3) / pageWidth) + 1;
    UIPageControl *pageController=[[UIPageControl alloc]init];
    [pageController setTag:page];
    
    [self action_imageSliderChange:pageController];

}

- (IBAction)action_imageSliderChange:(id)sender {
    
    UIPageControl *btnSender=(UIPageControl *)sender;
    if ([btnSender allControlEvents]) {
        [_scrollView setContentOffset:CGPointMake(btnSender.currentPage*_scrollView.frame.size.width, 0) animated:YES];
    }
    else{
        intPagintaion = btnSender.tag;
        [_pageControl setCurrentPage:btnSender.tag];
    }
}

- (IBAction)btnNextSlide:(UIButton *)sender {
    intPagintaion = intPagintaion+1;
    if (intPagintaion < 4) {
        
        if (intPagintaion==3) {
            _constSkip.constant=0;
            [_btnSwipe setTitle:@"GET STARTED" forState:UIControlStateNormal];
            _btnSkip.hidden=YES;
            
        }
        [_scrollView setContentOffset:CGPointMake(intPagintaion*_scrollView.frame.size.width, 0) animated:YES];
        [_pageControl setCurrentPage:intPagintaion];
    }
    else
    {
        SelectVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectVC"];
        [self.navigationController pushViewController:vc animated:true];
    }
}

- (IBAction)btnStartAction:(id)sender {
    
//   KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
//   [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
     SelectVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectVC"];
     self.navigationController.navigationBar.hidden=YES;
     [self.navigationController pushViewController:vc animated:true];
  
}



-(UIColor *)blend:(UIColor *)from  to:(UIColor *) to percent:(double) percent
{
    CGFloat fR = 0.0;
    CGFloat fG = 0.0;
    CGFloat fB = 0.0;
    CGFloat tR = 0.0;
    CGFloat tG = 0.0;
    CGFloat tB = 0.0;
    
    [from getRed:&fR green:&fG blue:&fB alpha:nil];
    [to getRed:&tR green:&tG blue:&tB alpha:nil];
    
    
    CGFloat dR = tR - fR;
    CGFloat dG = tG - fG;
    CGFloat dB = tB - fB;
    
    CGFloat rR = fR + dR * percent;
    CGFloat rG = fG + dG * percent;
    CGFloat rB = fB + dB * percent;
    
    return [UIColor colorWithRed:rR green:rG blue:rB alpha:1.0];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView  {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
    
    // horizontal
    CGFloat maximumHorizontalOffset = scrollView.contentSize.width - CGRectGetWidth(scrollView.frame);
    CGFloat currentHorizontalOffset = scrollView.contentOffset.x;
    
    // percentages
    CGFloat percentageHorizontalOffset = currentHorizontalOffset / maximumHorizontalOffset;
    
    NSLog(@"content offfset: %f", percentageHorizontalOffset);
    
    
    if (percentageHorizontalOffset < .25) {
        
              scrollView.backgroundColor = [self fadeFromColor:colorArray[0] toColor:colorArray[1] withPercentage:(percentageHorizontalOffset-0.12)*3];
       
   
    } else if (percentageHorizontalOffset >= .50 && percentageHorizontalOffset < .75) {
        scrollView.backgroundColor = [self fadeFromColor:colorArray[1] toColor:colorArray[2] withPercentage:(percentageHorizontalOffset-0.25)*3];
    } else if (percentageHorizontalOffset >= .75) {
        scrollView.backgroundColor = [self fadeFromColor:colorArray[2] toColor:colorArray[3] withPercentage:(percentageHorizontalOffset-0.50)*3];
    }
    self.lastContentOffset = scrollView.contentOffset.x;
}
-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    CGFloat pageWidth = self.scrollView.frame.size.width;
    
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    }

- (UIColor *)fadeFromColor:(UIColor *)fromColor toColor:(UIColor *)toColor withPercentage:(CGFloat)percentage
{
    // get the RGBA values from the colours
    CGFloat fromRed, fromGreen, fromBlue, fromAlpha;
    [fromColor getRed:&fromRed green:&fromGreen blue:&fromBlue alpha:&fromAlpha];
    
    CGFloat toRed, toGreen, toBlue, toAlpha;
    [toColor getRed:&toRed green:&toGreen blue:&toBlue alpha:&toAlpha];
    
    //calculate the actual RGBA values of the fade colour
    CGFloat red = (toRed - fromRed) * percentage + fromRed;
    CGFloat green = (toGreen - fromGreen) * percentage + fromGreen;
    CGFloat blue = (toBlue - fromBlue) * percentage + fromBlue;
    CGFloat alpha = (toAlpha - fromAlpha) * percentage + fromAlpha;
    
    // return the fade colour
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}


@end
