//
//  CellleftMenuTableViewCell.m
//  OPGNY
//
//  Created by Dheerendra chaturvedi on 19/02/17.
//  Copyright © 2017 Nilesh Pal. All rights reserved.
//

#import "CellleftMenuTableViewCell.h"

@implementation CellleftMenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setData:(NSMutableDictionary *)dictData
{
    [self.lblTitle setText:[dictData objectForKey:@"Title"]];
    [_imgTitle setImage:[UIImage imageNamed:[dictData objectForKey:@"Image"]]];
}

@end
