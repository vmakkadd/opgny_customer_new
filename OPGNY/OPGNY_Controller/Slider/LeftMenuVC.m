//
//  LeftMenuVC.m
//  text
//
//  Created by cis on 18/02/17.
//  Copyright © 2017 CIS. All rights reserved.
//

#import "LeftMenuVC.h"
#import "CellleftMenuTableViewCell.h"
#import "KYDrawerController.h"
#import "Constant.h"
#import "MyProfileVC.h"
#import "HomeVC.h"
//#import "HomeVC.h"
//#import "FeedbackVC.h"
//#import "ProfileVC.h"
#import "UIImageView+WebCache.h"
//#import "MyAgentVC.h"
//#import "LeftMenuLaseCell.h"
#import "LoginVC.h"
//#import "SuggestedVC.h"


enum {
    
    MHPhotoLibrary = 0,
    MHTakeFromCamera = 1
    
};

@interface LeftMenuVC () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>
{
    NSMutableArray *arrayMenu;
    
}
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader_Profile_Image;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UITableView *tblMenu;
@end

@implementation LeftMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _profileImg.layer.cornerRadius = _profileImg.frame.size.width / 2;
    _profileImg.clipsToBounds = YES;
    _profileImg.layer.borderWidth=2;
    _profileImg.layer.borderColor=[UIColor whiteColor].CGColor;
    
    self.navigationController.navigationBarHidden = YES;

}
- (void)viewWillAppear:(BOOL)animated {

    if ( [USER_PREF objectForKey:@"customer_id"] != nil)
    {
        arrayMenu = [[NSMutableArray alloc]initWithObjects:@{@"Title":@"EXPLORE",@"Image":@"explore_ic"},@{@"Title":@"LIKED",@"Image":@"like_ic"},@{@"Title":@"DISLIKED",@"Image":@"like_iclike_ic"},@{@"Title":@"SUGGESTED",@"Image":@"suggetion_ic"},@{@"Title":@"MY AGENT",@"Image":@"agent_ic"},@{@"Title":@"EDIT PROFILE",@"Image":@"edit_porfile_ic"},@{@"Title":@"FEEDBACK",@"Image":@"feedback_ic"}, nil];;
    }
    else{
        arrayMenu = [[NSMutableArray alloc]initWithObjects:@{@"Title":@"EXPLORE",@"Image":@"explore_ic"},@{@"Title":@"LIKED",@"Image":@"like_ic"},@{@"Title":@"DISLIKED",@"Image":@"like_iclike_ic"},@{@"Title":@"SUGGESTED",@"Image":@"suggetion_ic"},@{@"Title":@"MY AGENT",@"Image":@"agent_ic"},@{@"Title":@"EDIT PROFILE",@"Image":@"edit_porfile_ic"},@{@"Title":@"FEEDBACK",@"Image":@"feedback_ic"}, nil];
    }

    [_tblMenu reloadData];
    
//  NSDictionary *profileData = [USER_PREF objectForKey:@"user-details"];
    [self setData];
}

-(void)setData
{
    if([USER_PREF valueForKey:@"customer_id"] != nil )
    {
        NSDictionary *profileData = [USER_PREF objectForKey:@"user-details"];
        [_lblName setText:[NSString stringWithFormat:@"%@ %@",[profileData objectForKey:@"first_name"],[profileData objectForKey:@"last_name"]]];
        
        if ( [USER_PREF objectForKey:@"profile_picture"] == nil)
        {
         //   [self.profileImg setImage:[UIImage imageNamed:@"userName.png"]];
        }
        else{

            NSData* imageData = [USER_PREF objectForKey:@"profile_picture"];
            UIImage* image = [UIImage imageWithData:imageData];

          //  [self.profileImg setImage:image];
        }

        NSLog(@"%@",[NSString stringWithFormat:@"%@",[USER_PREF valueForKey:@"profile_picture_url"]]);
        
        _loader_Profile_Image.hidden=YES;

         NSString *urlstring = [NSString stringWithFormat:@"%@",[USER_PREF valueForKey:@"profile_picture_url"]];
        
        NSLog(@"%@",[urlstring stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
        
        urlstring =[urlstring stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString* webStringURL = [urlstring stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *url = [NSURL URLWithString:webStringURL];
    
        if (url!=nil) {
            
        /*    [self.profileImg sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"userName.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if (error == nil) {
                    [self.profileImg setImage:image];
                }
                else{
                  
                }
              
            }]; */
        }
        else{
            
        }
    }
    else{

        [_lblName setText:@"Guest User"];
        [_loader_Profile_Image stopAnimating];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)showLoginViewController
{
    KYDrawerController *drawer = (KYDrawerController *) [self.navigationController parentViewController];
            LoginVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
    
    //Closing drawer whene new controller appeares
    [USER_PREF setBool:NO forKey:@"isLoggedIn"];
     drawer.mainViewController=navController;
    [drawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:true];
    
    [USER_PREF removeObjectForKey:@"profile_picture_url"];
    [USER_PREF setBool:false forKey:@"is_agent_detail"];
    [USER_PREF removeObjectForKey:@"agent_detail"];
    [USER_PREF removeObjectForKey:@"user-details"];
    [USER_PREF removeObjectForKey:@"customer_id"];
}

#pragma mark - UIImagePicker delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    _profileImg.image = [info objectForKey:UIImagePickerControllerEditedImage];
     [self dismissViewControllerAnimated:YES completion:nil];
    
    [self callWebServiceToAddImage];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableView DataSource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrayMenu count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if ( indexPath.row == 7) {
//
//        static NSString *cellIdentifier = @"LeftMenuLaseCell";
//        LeftMenuLaseCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//        cell.frame = CGRectMake(-cell.frame.size.width, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
//
//
//        [UIView animateWithDuration:indexPath.row * 0.1 delay:indexPath.row * 0.1 options:UIViewAnimationOptionCurveEaseOut animations:^{
//            cell.frame = CGRectMake(0.0, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
//        } completion:nil];
//
        //[cell setData:[arrayMenu objectAtIndex:indexPath.row]];
//        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//
//        return cell;
//
//    }
    
//    else{
    
        static NSString *cellIdentifier = @"CellleftMenuTableViewCell";
        
        CellleftMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        cell.frame = CGRectMake(-cell.frame.size.width, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);

        [UIView animateWithDuration:indexPath.row * 0.1 delay:indexPath.row * 0.1 options:UIViewAnimationOptionCurveEaseOut animations:^{
            cell.frame = CGRectMake(0.0, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
        } completion:nil];

        [cell setData:[arrayMenu objectAtIndex:indexPath.row]];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
//    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    KYDrawerController *drawer = (KYDrawerController *) [self.navigationController parentViewController];
   
    [SharedInstance showAlertWithMessageForAction:@"User login is required to use all features of the application." withActionButtonName:@"SignIn" withCancelButtonName:@" Use as guest" handler:^(UIAlertAction *action) {
        [self showLoginViewController];
    }];
    
    switch (indexPath.row) {
        case 0:
        {
            HomeVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];

            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
            navController.navigationBar.hidden=YES;
            drawer.mainViewController=navController;

            [drawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:true];
            
        }
        break;
            case 1:
        {
            
            if ( [USER_PREF objectForKey:@"customer_id"] != nil)
            {
//                SuggestedVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SuggestedVC"];
//                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
//
//                viewController.strCheck =@"Liked";
//                //Closing drawer whene new controller appeares
//
//                drawer.mainViewController=navController;
//
//                [drawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:true];
            }
//            if ( [USER_PREF objectForKey:@"customer_id"] != nil)
//            {
//
//                HistoryListingVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HistoryListingVC"];
//
//                viewController.iFlag = 1;
//
//                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
//
//                //Closing drawer whene new controller appeares
//                drawer.mainViewController=navController;
//
//                [drawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:true];
//
//            }

        }
            break;
        case 2:
        {
//            if ( [USER_PREF objectForKey:@"customer_id"] != nil)
//            {
//
//                HistoryListingVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HistoryListingVC"];
//                viewController.iFlag = 2;
//
//                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
//
//                //Closing drawer whene new controller appeares
//
//                drawer.mainViewController=navController;
//                [drawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:true];
//            }

            if ( [USER_PREF objectForKey:@"customer_id"] != nil)
            {
//                SuggestedVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SuggestedVC"];
//                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
//
//                //Closing drawer whene new controller appeares
//                viewController.strCheck =@"Disliked";
//                drawer.mainViewController=navController;
//
//                [drawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:true];
            }
  
        }
            break;
        case 3:
        {
            if ( [USER_PREF objectForKey:@"customer_id"] != nil)
            {
//                SuggestedVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SuggestedVC"];
//                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
//
//                //Closing drawer whene new controller appeares
//                viewController.strCheck =@"Suggested";
//
//                drawer.mainViewController=navController;
//
//                [drawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:true];
            }

        }
            break;
        case 4:
        {
            
            if ( [USER_PREF objectForKey:@"customer_id"] != nil)
            {
            
                MyProfileVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileVC"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
                navController.navigationBar.hidden=YES;

                //Closing drawer whene new controller appeares
                drawer.mainViewController=navController;
                [drawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:true];
                
            }
        }
            break;
        case 5:
        {
            if ( [USER_PREF objectForKey:@"customer_id"] != nil)
            {
//                ProfileVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
//
//                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
//
//                //Closing drawer whene new controller appeares
//
//                drawer.mainViewController=navController;
//                [drawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:true];
            }

        }
            break;
            
        case 6:
        {
//            FeedbackVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedbackVC"];
//
//            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
//
//            //Closing drawer whene new controller appeares
//
//            drawer.mainViewController=navController;
//            [drawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:true];
        }
            break;
        case 7:
        {
            //[self updateLocalPref];
//            LoginVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
//
//            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
//
//            //Closing drawer whene new controller appeares
//            [USER_PREF setBool:NO forKey:@"isLoggedIn"];
//            drawer.mainViewController=navController;
//            [drawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:true];
//
//            [USER_PREF removeObjectForKey:@"profile_picture_url"];
//            [USER_PREF setBool:false forKey:@"is_agent_detail"];
//            [USER_PREF removeObjectForKey:@"agent_detail"];
//            [USER_PREF removeObjectForKey:@"user-details"];
//            [USER_PREF removeObjectForKey:@"customer_id"];
        }
        break;
            
        default:
            break;
    }
    
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)callWebServiceToAddImage {
    
    NSString *strURL=[NSString stringWithFormat:@"%@",POST_UPDATE_PROFILE_IMAGE];
    
    NSDictionary *dict = @{@"customer_id":[USER_PREF objectForKey:@"customer_id"],@"profile_picture":_profileImg.image};
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = true;
//    [SharedInstance callMultipartWebService:dict andURL:strURL andView:self.view andgetData:^(NSDictionary *data, NSError *error) {
//        [UIApplication sharedApplication].networkActivityIndicatorVisible = false;
//        if (data) {
//            [MBProgressHUD hideHUDForView:self.view animated:true];
//            NSLog(@"data %@",data);
//            if ([data[@"status"] isEqualToString:@"success"]) {
//                
//                [USER_PREF setValue:data[@"result"][0][@"profile_picture_url"] forKey:@"profile_picture_url"];
//                [USER_PREF synchronize];
//                
//                [_loader_Profile_Image startAnimating];
//                if ([NSURL URLWithString:[USER_PREF objectForKey:@"profile_picture_url"]] !=nil) {
//                    [_profileImg sd_setImageWithURL:[NSURL URLWithString:[USER_PREF objectForKey:@"profile_picture_url"]] placeholderImage:[UIImage imageNamed:@"add.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
//                        if (error == nil) {
//                            [_profileImg setImage:image];
//                        }
//                        else{
//                            [_profileImg setImage:[UIImage imageNamed:@"add.png"]];
//                         }
//                        [_loader_Profile_Image stopAnimating];
//                    }];
//                }
//                else{
//                    [_profileImg setImage:[UIImage imageNamed:@"add.png"]];
//                 }
//                
//            }
//            else {
//                [SharedInstance showAlertWithMessage:data[@"result"]];
//            }
//        }
//        else {
//            [SharedInstance showAlertWithMessage:@"Something went wrong. Please try again!"];
//        }
//        
//    }];
    
}

- (IBAction)btnCloseAction:(id)sender {
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:YES];
}

- (void)updateLocalPref {
    [USER_PREF setObject:@"2" forKey:@"bedrooms"];
    [USER_PREF setObject:@"2" forKey:@"bathrooms"];
    [USER_PREF setValue:@"" forKey:@"max_price"];
    [USER_PREF setValue:@"0" forKey:@"min_price"];
    [USER_PREF setObject:@"" forKey:@"area"];
    [USER_PREF setValue:@"" forKey:@"sa_menu"];
    [USER_PREF setObject:@"" forKey:@"neighborhood"];
    [USER_PREF synchronize];
}
- (IBAction)btnTakePhoto:(id)sender {

    if ( [USER_PREF objectForKey:@"customer_id"] != nil)
    {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];

        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

            // Cancel button tappped do nothing.

        }]];

        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take a new photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

            // take photo button tapped.
            [self showCameraToTakePic];

        }]];

        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo from gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

            // choose photo button tapped.
            [self letUserToChooseFromPhotoLibrary];

        }]];
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
    else{
        [SharedInstance showAlertWithMessageForAction:@"User login is required to use all features of the application." withActionButtonName:@"SignIn" withCancelButtonName:@" Use as guest" handler:^(UIAlertAction *action) {
            [self showLoginViewController];
        }];
    }

}
-(void)showCameraToTakePic{
    UIImagePickerController *objController = [[UIImagePickerController alloc]init];
    objController.delegate = self;
    objController.allowsEditing = YES;
    objController.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:objController animated:YES completion:nil];
}

-(void)letUserToChooseFromPhotoLibrary{
    UIImagePickerController *objController = [[UIImagePickerController alloc]init];
    objController.delegate = self;
    objController.allowsEditing = YES;
    objController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:objController animated:YES completion:nil];
}

@end
