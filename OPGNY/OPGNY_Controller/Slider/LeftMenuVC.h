//
//  LeftMenuVC.h
//  text
//
//  Created by cis on 18/02/17.
//  Copyright © 2017 CIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
@interface LeftMenuVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileName;


@end
