//
//  CellleftMenuTableViewCell.h
//  OPGNY
//
//  Created by Dheerendra chaturvedi on 19/02/17.
//  Copyright © 2017 Nilesh Pal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellleftMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
-(void)setData:(NSMutableDictionary *)dictData;
@end

