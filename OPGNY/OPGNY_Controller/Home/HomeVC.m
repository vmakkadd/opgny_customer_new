//
//  HomeVC.m
//  OPGNY
//
//  Created by Nishat Ansari on 12/05/18.
//  Copyright © 2018 WebHungers. All rights reserved.
//

#import "HomeVC.h"
#import "KYDrawerController.h"

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)clickBack:(id)sender {
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
}
- (IBAction)clickChangeSearchFilter:(id)sender {
    
}
- (IBAction)clickAskAgent:(id)sender {
    
}

@end
